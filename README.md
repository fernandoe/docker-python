# docker-python

Dockerfile for python 2


## Release Notes

### 0.0.4

* Install libffi-dev and libssl-dev to support the fabric installation using pip.


### 0.0.3

* Python 2.7.6


### 0.0.2

* Python 2.7.6


### 0.0.1

* Python 2.7.6



## Example Commands ##

```shell
docker build -t local/python .
docker run --rm -i -t fernandoe/docker-python:0.0.1 /bin/bash
```